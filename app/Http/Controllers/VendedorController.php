<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\VendedorModel as Vendedor;

class VendedorController extends Controller
{
	public function index()
	{
		return Vendedor::where(function($query){
				$query->where('CODIGOIDENTIFICACAO', '<>', 0);
			})->get(['CODIGO', 'NOME', 'CODIGOIDENTIFICACAO']);
	}
}