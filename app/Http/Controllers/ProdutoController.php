<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProdutoModel as Produto;

class ProdutoController extends Controller
{
	public function show($id)
	{
		$produto = Produto::where('GRUPO', $id)
						  ->get(['CODIGO', 'DESCRICAO', 'PRVENDA']);
		return $produto;
	}
}