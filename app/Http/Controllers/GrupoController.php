<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\GrupoModel as Grupo;

class GrupoController extends Controller
{
	public function index()
	{
		$grupos = Grupo::gruposComProdutos();

		return $grupos;
	}
}