<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OrcamentoModel as Orcamento;

class OrcamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $orcamento = Orcamento::where('NUMERO', $id)->get(['NUMERO', 'SEQUENCIA', 'CODIGO_PRODUTO', 'DESCRICAO', 'QUANTIDADE', 'TOTALITEM', 'TEXTO_SERVICO', 'PRVENDA']);
        if(count($orcamento) > 0){
            return $orcamento;
        }else{
            $arr = array('NUMERO' => 'vazio');
            return json_encode($arr);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $orcamentoCadastrado = Orcamento::create($attributes);

        if($orcamentoCadastrado){
            $arr = array('cadastrado' => 'true');
            return json_encode($arr);
        }else{
            $arr = array('cadastrado' => 'false');
            return json_encode($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $numero = $request->input('NUMERO');
        $numeroAlt = $request->input('NUMEROALT');

        $updateOrcamento = Orcamento::where('NUMERO', $numero)
                                    ->update(['NUMERO' => $numeroAlt]);

        if($updateOrcamento){
            $arr = array('registroAlterado' => count($updateOrcamento));
            return json_encode($arr);
        }else{
            $arr = array('registroAlterado' => count($updateOrcamento));
            return json_encode($arr);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $orcamento = Orcamento::where('NUMERO', $request->input('NUMERO'))
                              ->where('SEQUENCIA', $request->input('SEQUENCIA'))
                              ->where('CODIGO_PRODUTO', $request->input('CODIGO_PRODUTO'))
                              ->delete();
        $arr = array('registrosDeletados' => $orcamento);
        return json_encode($arr);
    }

    public function destroyWithFecorca($id)
    {
        $orcamento = Orcamento::where('NUMERO', $id)->delete();
        $arr = array('registrosDeletados' => $orcamento);
        return json_encode($arr);
    }

    public function verifyLastSequence($id)
    {
        $orcamento = Orcamento::where('NUMERO', $id)
                                ->orderBy('SEQUENCIA', 'desc')
                                ->first(['SEQUENCIA']);

        if(count($orcamento) > 0){
            return $orcamento;
        }else {
            $arr = array('SEQUENCIA' => 'VAZIO');
            return json_encode($arr);
        }
        
    }
}
