<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\FecorcaModel as Fecorca;

class FecorcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Fecorca::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = [
           'NUMERO'        => $request->input('NUMERO'),
           'NOME'          => "",
           'DATA'          => $request->input('DATA'),
           'HORA'          => $request->input('HORA'),
           'CLIENTE'       => "0",
           'VENDEDOR'      => $request->input('VENDEDOR'),
           'PER_ACRESCIMO' => $request->input('PER_ACRESCIMO'),
           'MESAABERTA'    => "SIM"
        ];

        $fecorcaCadastrado = Fecorca::create($attributes);

        if($fecorcaCadastrado){
            $arr = array('cadastrado' => 'true');
            return json_encode($arr);
        }else{
            $arr = array('cadastrado' => 'false');
            return json_encode($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fecorca = Fecorca::where('numero', $id)->first();

        if(count($fecorca) > 0){
            return $fecorca;
        }else{
            $arr = array('NUMERO' => 'vazio');
            return json_encode($arr);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $numero = $request->input('NUMERO');
        $mesaFecha = $request->input('MESAFECHA');

        $updateFecorca = Fecorca::where('NUMERO', $numero)
                                ->update(['MESAFECHA' => $mesaFecha]);

        if($updateFecorca){
            $arr = array('registroAlterado' => count($updateFecorca));
            return json_encode($arr);
        }else{
            $arr = array('registroAlterado' => count($updateFecorca));
            return json_encode($arr);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fecorca = Fecorca::where('NUMERO', $id)->delete();
        $arr = array('registrosDeletados' => $fecorca);
        return json_encode($arr);
    }

    public function updateSwitch(Request $request) 
    {
        $numero = $request->input('NUMERO');
        $numeroAlt = $request->input('NUMEROALT');

        $updateFecorca = Fecorca::where('NUMERO', $numero)
                                ->update(['NUMERO' => $numeroAlt]);

        if($updateFecorca){
            $arr = array('registroAlterado' => count($updateFecorca));
            return json_encode($arr);
        }else{
            $arr = array('registroAlterado' => count($updateFecorca));
            return json_encode($arr);
        }
    }
}
