<?php
Route::group(['prefix' => 'api/v1/', 'middleware' => 'cors'], function(){

	Route::get('vendedor', 'VendedorController@index');
	
	Route::get('fecorca', 'FecorcaController@index');
	Route::post('fecorca/create', 'FecorcaController@store');
	Route::get('fecorca/{id}', 'FecorcaController@show');
	Route::delete('fecorca/{id}', 'FecorcaController@destroy');
	Route::post('fecorca/update', 'FecorcaController@update');
	Route::post('fecorca/updateswitch', 'FecorcaController@updateSwitch');

	Route::get('orcamento/{id}', 'OrcamentoController@index');
	Route::post('orcamento/create', 'OrcamentoController@store');
	Route::post('orcamento/update', 'OrcamentoController@update');
	Route::post('orcamento/fecorca/{id}', 'OrcamentoController@destroyWithFecorca');
	Route::post('orcamento/verifysequence/{id}', 'OrcamentoController@verifyLastSequence');
	Route::post('orcamento/destroy', 'OrcamentoController@destroy');
	
	Route::get('qtdmesas', 'Param2Controller@qtdMesas');

	Route::get('grupos', 'GrupoController@index');

	Route::get('produtos/{id}', 'ProdutoController@show');
});

