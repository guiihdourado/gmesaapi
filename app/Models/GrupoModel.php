<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoModel extends Model
{
    protected $table = "grupos";
    protected $guarded = [];
    public $timestamps = false;

    public static function gruposComProdutos() {
    	return \DB::select(\DB::raw("
			select 
			count(produtos.grupo) as total, 
			produtos.grupo, 
			coalesce(grupos.NOME_GRUPO,'PRODUTOS SEM GRUPO') as nome_grupo 
			from produtos 
			left join grupos on grupos.grupo=produtos.grupo
			where produtos.controlemesas = 'SIM'
			group by produtos.grupo
    	"));
    }
}
