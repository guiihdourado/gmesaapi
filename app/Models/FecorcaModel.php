<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FecorcaModel extends Model
{
    protected $table = "fecorca";
    protected $guarded = [];
    public $timestamps = false;
}
