<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Param2Model extends Model
{
    protected $table = "param2";
    protected $guarded = [];
    public $timestamps = false;
}
