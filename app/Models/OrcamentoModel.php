<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrcamentoModel extends Model
{
    protected $table = "orcamento";
    protected $guarded = [];
    public $timestamps = false;
}
