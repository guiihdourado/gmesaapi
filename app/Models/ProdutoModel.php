<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoModel extends Model
{
    protected $table = "produtos";
    protected $guarded = [];
    public $timestamps = false;
}
