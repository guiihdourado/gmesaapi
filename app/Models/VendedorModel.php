<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendedorModel extends Model
{
    protected $table = "vendedor";
    protected $guarded = [];
    public $timestamps = false;
}
